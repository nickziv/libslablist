`libslablist` is an implementation of slab lists.

Slab lists are a memory efficient data structure, that can optionally keep data
sorted in logarithmic time.

I designed slab lists with quality and performance in mind. I had a lot of code
using ad-hoc linked lists and pre-canned AVL Trees at the same time. While
these are good solutions to well-known problems, I was tired of (a) using two
different structures for the same data and (b) not being able to hold all of my
data in memory --- I had to search it by chunks. So slab lists address both of
those problems. They can be used for sorted and unsorted data, which allows us
to use the same umem caches for both. They are also 4x to 5x more memory
efficient than AVL Trees, allowing me to reduce the number of times I go to
disk.

See the block comment at the top of src/slablist_impl.h for implementation
overview.

The `libslablist` API is not yet stable. It is volatile and may change.

All of the code in the library follows the Illumos kernel coding style,
sometimes referred to as BJNF (Bill Joy Normal Form).

To install:

	cd build/illumos
	make install

Support for FreeBSD is forthcoming.

Here are some comparisons, so that you can get an idea of how Slab Lists
perform. First we will compare to `uuavl`. `uuavl` is the AVL Tree
implementation used in the Illumos kernel. It is among the most
memory-efficient and cpu-efficient implementations in the world.

`uuavl` uses 10% _less_ time than `libslablist` on sequential input.
`uuavl` uses 365% _more_ memory than `libslablist` on sequential input.
`uuavl` uses 7% _less_ time than `libslablist` on random input.
`uuavl` uses 271% _more_ memory than `libslablist` on random input.

Other AVL Tree implementations, such as those found in GNU libavl use 50% to
100% more memory than `uuavl`. Which makes them half as competitive as `uuavl`.
GNU libavl implementations also perform as well as `libslablist`.

We have a bunch of foreign data structure implementations that we use to
evaluate Slab List performance. Most of these are self-contained and can be
built from this tree. Others are simply too large to be included, or were
designed as shared objects. These include: libuutil, libredblack, and myskl.
You'll have to fetch them and install them manually.

We also have a test suite that uses DTrace to verify the consistency of
libslablist's internal state. These tests are located in the `tests` directory.
They are usable like so:

	dtrace -c 'build/drv_gen ...' -L <libpath> -s tests/<script-name>.d

By default, libslablist uses the following library path for DTrace libraries:
/opt/libslablist/include/dtrace.
